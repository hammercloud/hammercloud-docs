# HammerCloud extension - `celery_prod` mode
  * [Normal priority jobs](./celery_prod/) (`celery_prod` mode)
  * [Low priority jobs](./celery_prod_lowpriority/) (`celery_prod_lowpriority` mode)
  * [Template file configuration](./celery_prod/#template-file-configuration)
  * [Template migration](./celery_prod/#template-migration)
  * [Job priority](./celery_prod/#job-priority)
  * [Override template configuration with extraargs](./celery_prod/#override-template-configuration-with-extraargs)
