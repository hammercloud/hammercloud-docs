# Info for new team members


## Introduction 

This page contains a set of resources that may be useful to a new member of the HammerCloud team. Any information, in particular specific ones such as directory/file paths or service node names, are subject to change over time and is not guaranteed to be fully accurate depending on when you're reading this page. 

 
##  E-mail notifications from HammerCloud 

HammerCloud will send out several e-mail notifictions to inform about events/failures that are occuring. The notifications are send to the ``hammercloud-notifications-atlas``, ``hammercloud-notifications-atlas-es`` and ``hammercloud-notifications-atlas-es-digest`` lists (the latter two are only concerning EventService Blacklisting/Whitelisting). 


##  AFT/PFT Blacklisting/Whitelisting notifications (hammercloud-notifications-atlas) 

The messages that are send to this list can be roughly split into five categories:

 * Notification of blacklisting/whitelisting of a Panda Queue (filter: "Auto-Excluded" or "reset online" in subject): this is a mail that is send out for each Panda Queue that is blacklisted/whitelisted each blacklisting cycle. In case of a blacklisting notification, additional information is enclosed in the mail regarding the exclusion reason. Mails regarding AFTs/PFTs have "[ANALYSIS]"/"[PROD]" in their subject.
 * Blacklisting reports (filter: "Blacklisting Report" in subject): these are send out with each blacklisting cycle, summarising what happened in terms of new blacklisting/whitelisting actions in the cycle and give general information about the status of the active AFTs/PFTs, such as sites that are currently in TEST state because of HC or if the different AFTs/PFTs have enough jobs for proper blacklisting/whitelisting decision making.
 * Agis update notifications (filter: "[HC-UPDATE-FROM-AGIS]" in subject): these are concerning everything related to the AGIS data collection that HammerCloud is running. Any updates in AGIS that are relevant for HammerCloud (i.e. that lead to updates in the internal DB) will be notified, but also failures of the AGIS update script (filter: "AGIS update script failed" in subject). The latter is not necessarily a problem, several issues can just be transient (e.g. connection issues to the AGIS server). Only if the AGIS updating fails continuously it warrants some investigation.
 * Missing datasets notifications (filter: "Sites missing datasets" in subject): these are send out when sites that are configured to receive AFT/PFT jobs don't have the necessary datasets replicated to one of their associated storage elements. ATLAS DDM is also informed about any missing datasets and will take action to ensure the replica creation on the respective storage elements, so usually this doesn't require actions from HammerCloud operations.
 * Panda Queue injection into running tests (filter: "[HC-INJECT-PQ-TO-BLACKLIST-TEST]" in subject): these will be send out when panda queues are added to already running AFTs/PFTs. 

 
##  EventService Blacklisting/Whitelisting notifications (hammercloud-notifications-atlas-es and hammercloud-notifications-atlas-es-digest) 
 
 The EventService messages are very similar to the AFT/PFT ones in terms of when they are send out:
 
 * Notification of EventService blacklisting/whitelisting of a Panda Queue (filter: "Auto-Excluded" or "reset online" in subject)
 * Blacklisting reports (filter: "Event Service Blacklist Report" in subject): these are send out hourly, not every 30 minutes as for AFT/PFT reports.
 * EventService blacklisting digests (hammercloud-notifications-atlas-es-digest): in addition to the hourly reports, these digests are send out daily. 
     
 
##  HammerCloud operations 

###  HammerCloud admin web interface 

You get to the HammerCloud admin web interface by clicking on the **Administration** button in the top right of the [HammerCloud web page](https://hammercloud.cern.ch/hc/admin/atlas/). You'll be prompted to authenticate yourself with CERN SSO. If have the proper permissions you'll end up on the HammerCloud admin web page. Two tabs are particularly relevant for the general HammerCloud operations, **Templates** and **Tests**. 

####  Templates
 
  Here you find the web interface template configurations of all HammerCloud tests. On the configuration page of a template (click on the template ID) you'll find the following configuration fields:

 * **Type information**: here you define whether the test should be **stress** or **functional**, the **Lifetime** and **Period** of the tests, and switches to mark a test as **active** and/or **golden** (which is only relevant for functional tests)
    * **stress**: tests for this template are only started manually.
    * **functional**: tests for this template are started automatically by HammerCloud, if the template is set to **active**. Tests run periodically with the lifetime (in days) configured in **Lifetime**. While the test is running, jobs are submitted continuously with a gap between finishing of the previous job and submitting of the next one, as defined in **Period** (in seconds). If a **functional** test template is marked as **golden**, it will be taken into account for PFT/AFT/PFT_MCORE testing (if the **Description** field properly indicates for which one).
    * **Description** : If "AFT ", "PFT ", or "PFT_MCORE " (the trailing space is important!) is in the description field, HammerCloud will modify the **TEMPLATE SITES** list before each test run so that it's up-to-date with the current AGIS HammerCloud configurations. 
 * **Files**: here you specify the template files needed for the test as well as some additional configurations. Regarding what you have to set for the configurations, refer to other templates that are currently in use.
    * **Jobtemplate**: this is the file path to the template .tpl file, as it can be found on the submission node. It must be relative to the input file base directory on the node (/data/hc/apps/atlas/inputfiles/templates/). If the file is not yet in the selection list, it can be added with the + symbol next to the list.
    * **Usercode**: this is the file path to the (optional) tarball, which is send to PanDA with the job submission, if the HammerCloud template is configured with no build job. This also must be relative to the input file base directory.
    * **Extraargs**: this field can be used to overwrite configurations in the .tpl file, without having to change the file itself. 
 * **TEMPLATE HOSTS**: this is the list of submission nodes a new test for this template can be scheduled to. You must make sure that each node in this list has the files you specified under Files in its input file base directory.
 * **TEMPLATE SITES**: this is the list of computing sites to which test jobs are submitted to. All sites specified here must have an associated storage element, where the input dataset for this template is replicated to.
 * **TEMPLATE DSPATTERNS**: here you specify the dataset pattern(s) of the input dataset(s) of the template. These must match the ones you specified in the .tpl file. 
 
####  Tests
Here you find all tests created by HammerCloud, with which template configuration they were created with (TEMPLATE column), and links to the test web pages (URL column). You can add a new test with the Add Test button in the top right. On the **Add test** page you must specify the start time (some time in the future, like 5 minutes) and end time, as well as the template for the test. Note that the template must be category **stress** for manually started tests. 

###  HammerCloud test templates 

 HammerCloud tests are defined in two steps:

 * The test template .tpl file and auxiliary files on the HammerCloud submission nodes (base directory ``/data/hc/apps/atlas/inputfiles/templates/``)
 * The test template in the HammerCloud web interface 

The template files are generally stored in the [atlas inputfiles gitlab repository](https://gitlab.cern.ch/hammercloud/hammercloud-atlas-inputfiles) and are distributed to all submission nodes via RPM package installation. 

####  Developing a new template 

 If you want to create a new template, you should in general follow these steps:

 * Find/create a PanDA job that does whatever you want to test and create a .tpl file from it, following the instructions in [hcdocs](https://hammercloud.docs.cern.ch/atlas-ops/celery_prod/celery_prod/#template-file-configuration). In particular, you can generate a draft template file from the job directly using [configurejob.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas-software/tree/master/celery_utils/configurejob). Alternatively, you can use the .tpl file of an already existing template and change it to your needs.
 * Put the .tpl file on one of the HammerCloud dev nodes (hammercloud-ai-11 or hammercloud-ai-12) somewhere in the input file base directory (/data/hc/apps/atlas/inputfiles/templates/). On the dev nodes you should have sudo privileges, which allow you to directly change the content of this directory (but be careful to not interfere with other templates!). This allows for a proper testing of your new template.
 * Set up the template configuration in the HammerCloud template interface (have a look at the respective section of this page). Initially you want to set the category of the template to stress, so that you can manually start tests for it.
 * Start a test for the template, check the result, fix issues/bugs in the .tpl file or template configuration, rinse and repeat. If you're convinced that the template now works as expected, proceed with whatever you wanted to do with the template.
 * If the template is not meant for some quick testing only, it probably makes sense to properly include it in the [atlas inputfiles gitlab repository](https://gitlab.cern.ch/hammercloud/hammercloud-atlas-inputfiles) and build/install a new RPM package.
    * First, commit the .tpl file and any auxiliary file to a fitting place in the repository (the structure in the repository reflects the one that is on the submission nodes).
    * Build the RPM according to the instructions [here](https://twiki.cern.ch/twiki/bin/viewauth/IT/HammerCloudAgile#Release_a_new_version_of_the_pac). It might be necessary that you follow the configuration instructions [here](https://cern.service-now.com/service-portal/article.do?n=KB0005632) first.
    * Open a ticket to hammercloud-tickets to request deployment of the new RPM to all submission nodes. 
 * If the template is meant for a new AFT or PFT, have a look at these instructions [here](https://hammercloud.docs.cern.ch/atlas-ops/aft_pft_commissioning_decommissioning/) as well. 
 * If the template is meant to be used for auto-exclusion, a corresponding ALRB template needs to be created and activated ([instructions](https://hammercloud.docs.cern.ch/atlas-ops/ALRBdevel/)) and a sibling needs to be created here: [https://hammercloud.cern.ch/hc/admin/atlas/templatesibling/](https://hammercloud.cern.ch/hc/admin/atlas/templatesibling/)
    

###  Troubleshooting

####  AFT/PFT blacklisting reports 

 As mentioned earlier, the blacklisting reports will give you a summary of what happened in each blacklisting cycle. These should be checked from time to time to see if there are some issues in the HammerCloud test operations. Mostly two aspects are relevant for this, sites that currently are in test due to blacklisting and sites for which too few jobs are available for a given AFT/PFT template in order to make a blacklisting decision.

In general, it is not unexpected to see some sites showing up in these lists. This doesn't necessarily mean that something is wrong. Here are some scenarios where investigation may be needed:

  * Sites in test: if this list gets very long in a short amount of time, this indicates that there is a general issue with one or more services that are required by PanDA jobs. For example, if the rucio servers are not responding to incoming queries, all jobs fail since they can't get their input or write their output. This scenario will also lead to Auto-Exclusion alerts.
  * Sites with too few jobs for a given template: this can just mean that PanDA queues are very busy or that there is a problem with PanDA which leads to non-processing (i.e. explicitly not failing or succeeding) jobs. However, especially if there is a strong bias towards one of the three AFT/PFT templates (i.e. missing jobs only for one of the templates for a large amount of sites), the job submission of HammerCloud should be checked. 

If you want to dig deeper in suspicious tests, you should look at their respective test page on the HammerCloud web page. You can either select it from the [front page](http://hammercloud.cern.ch/hc/app/atlas/), or you go to the Tests web page under HammerCloud admin. On the test page (e.g. [this one](http://hammercloud.cern.ch/hc/app/atlas/test/20128750/)), you'll find general information about the test, such as which template the test responds to, it's start and end time, submitted/running/finished/failed jobs, etc.. Under the **Sites** tab, there is a summary of all test jobs per site. Grayed out sites are omitted in this test. You can check the reason by clicking on the **View Test Directory (for debugging)** link where the stderr output of the submission script for this test as well as a "jobs" folder. In the folder, there is one submission script for each PanDA queue. If HammerCloud detected an issue during the test setup, which prevents the submission for one of the queues, the error reason is appended to the file name which also prevents the submission for this particular queue. 


####  Issues with one/a few PanDA queue(s) 
 Usually, experts responsible for the particular PanDA queue(s) will send an e-mail to the HammerCloud support list if e.g. no test jobs are send to the queue or the status of the queue is not changed by HammerCloud even though jobs are running and finishing/failing. There are several places to investigate if these kinds of problems come up (this is a non-exhaustive list of course):

 * Check the AGIS configuration for the particular queue, e.g. [this one](http://atlas-agis.cern.ch/agis/pandaqueue/detail/ANALY_LRZ/full/). In order to receive HammerCloud test jobs, **hc_param** and **hc_suite** must be properly configured and the site must not be in status BROKEROFF or OFFLINE. If it should also be blacklisted/whitelisted automatically by HammerCloud, one PanDA queue with **is_default** set to true must exist in the respective ATLAS site for the type of test (i.e. there must be exactly one queue for AFT and PFT respectively for which ``is_default = true`` for each ATLAS site).
 * You can also have a look at these [blacklisting overview slides](https://indico.cern.ch/event/692124/contributions/2899914/attachments/1611506/2559099/BlacklistingOverview_ATLASSiteJamboree20180306_jschovan.pdf) for more details on PanDA queue configuration.
 * A more direct way of checking how HammerCloud sees the configuration of a given PanDA queue is to look at the information collected [here](https://hc-ai-core.cern.ch/testdirs/atlas/agis_pandaresources.json). This is the relevant AGIS information collected by HammerCloud. On the top right is a filter to select only json entries where the specified string is present, or you can do a text search in the Raw Data tab.
 * [This webpage](https://hc-ai-core.cern.ch/testdirs/atlas/check_agis_config.html) also shows possibly misconfigured queues with some pre-defined suggestions on what is missing/wrong in the configurations.
    In case of wrong blacklisting/whitelisting behaviour, you should also have a look at the CRIC [probe logs](https://atlas-cric.cern.ch/atlas/pandaqueuestatushistory/list/).
 * Also, you might want to look at/point to the FAQ section [here](https://twiki.cern.ch/twiki/bin/viewauth/IT/HammerCloudTutorialATLASsiteAdmins#5_QAs). Very common questions that come up (and the usual answers to them) are documented there. 

####  Massive Blacklistings 
Blacklisting of a large amount of PanDA queues in a short amount of time usually means that there is a problem with a central service or some other systematic issues leading to failing jobs. Usually, you'll notice such an event from Auto-Exclusion alerts send out by HammerCloud (something like 30-60 blacklistings within the span of 1h). In this case, communication with the other relevant ADC domains (like PanDA or DDM experts) is needed to figure out what went wrong. Also, normally it's necessary to suspend the HammerCloud probing for a while, which is done here (assuming you have the right permissions). The experts (outside of HammerCloud operations) who usually decide whether HammerCloud probing should be suspended or not also have the privilege to change this, so it's not necessarily something that you need to take care of (but you should make sure that you get the necessary permissions, just in case!). After the HammerCloud probing is suspended you usually want to revert the blacklistings, for which instructions are [here](https://hammercloud.docs.cern.ch/atlas-ops/massive_blacklisting/). 

## Database

Try out your database access on lxplus and change your password:
```
# mysql -h dbod-hc-atlas.cern.ch  -u $USER -P 5504 -p
mysql> SET PASSWORD = PASSWORD('new_password');
```

**Be very careful** when handling the database, you are working on a production database. Ask someone if in doubt.

These may come handy:
```
show databases;
show tables;
describe TABLENAME;
```

Get list of jobs of a test:
```
select * from result where test=TESTID;
```

Remove sites from a template:
```
delete from template_site  where template=957;
``` 
this is the PFT template, CRIC collector should return the sites within 2 minutes.

Remove sites from a test: 

**WARNING: NOT AFTER APPROVAL. Do not touch sites of a test that is not in draft status!!!**
```
delete from test_site  where test=11;
```

Get some information about tests of certain properties:
```
select id, state, template, starttime, endtime
from test
where starttime>'2019-03-15' and template=1013
```

Set a test to fail:
```
update test
set state='error', endtime='2019-03-16 15:00:00'
where id in (1234, 5678);
```

Move a set of templates to a specific host
```
select id from host where name='hammercloud-ai-XY.cern.ch; 
update template_host set host=HOST_ID where template in (TEMPLATE_ID1,TEMPLATE_ID2,...); 
```

Get some debugging info about jobs that fail with tarball upload error:
```
select * from (
select backendID, ganga_status as jobstatus, exit_status_1, exit_status_2, mtime, site, TIMESTAMPDIFF(SECOND, start_time, stop_time) as tdiff,
reason from result where mtime>'2021-10-16 00:00:00'
and reason like '%User tarball (source unknown) cannot be downloaded from PanDA server%'
-- and reason like '%Job failed: Non-zero failed job return code: 1%'
-- and reason like '%is missing in pilot XML%'
and ganga_status='f'
order by mtime desc
)
R join (select id, name from site) S on R.site=S.id limit 10;
```

There is a docs [how to set up tunnel](https://twiki.cern.ch/twiki/bin/viewauth/IT/HammerCloudAgile#Accessing_local_database_from_ou), so that one can access DB from outside CERN perimeter firewall, it comes handy e.g. when using DBeaver or MySQL workbench or similar tool.

## Development within submission loop

Keep in mind that celery is used for test generation and job submission. In order to test changes withing the submission loop, the celery deamon needs to be restarted and the .pyc files of the python files you changed need to be deleted.
In order to restart the celerey deamon, run the following as sudo:
```
service celeryd restart
```
When testing test generation, restarting the test that is running on the dev node by killing or ending it and waiting on it to be rescheduled can be time-consuming. The following short-cut can make development more efficient. Starting point is that there is already a test in running state on the dev node. Then you can, instead of using the web-interface, set the test to 'scheduled' in the database directly.

Connect to the database:
```
mysql -h dbod-hc-atlas.cern.ch  -u $USER -P 5504 -p
```

select the database:
```
USE dev_atlas;
```

set the state of the given test:
```
update test
set state='scheduled'
where id=XXXXX;
```


## Links
   * HammerCloud website: [http://hammercloud.cern.ch/hc/app/atlas/](http://hammercloud.cern.ch/hc/app/atlas/)
   * HammerCloud MkDocs docs page: [https://hammercloud.docs.cern.ch](https://hammercloud.docs.cern.ch)
   * Current AGIS/CRIC information collected by HammerCloud: [https://hc-ai-core.cern.ch/testdirs/atlas/agis_pandaresources.json](https://hc-ai-core.cern.ch/testdirs/atlas/agis_pandaresources.json)
   * AGIS configuration check webpage: [https://hc-ai-core.cern.ch/testdirs/atlas/check_agis_config.html](https://hc-ai-core.cern.ch/testdirs/atlas/check_agis_config.html)
   * CRIC probe setting log: [https://atlas-cric.cern.ch/atlas/pandaqueuestatushistory/list/](https://atlas-cric.cern.ch/atlas/pandaqueuestatushistory/list/)
   * CRIC PanDA blacklisting overview: [https://atlas-cric.cern.ch/atlas/pandaqueuestatus/list/](https://atlas-cric.cern.ch/atlas/pandaqueuestatus/list/)
   * HammerCloud Kibana: [https://monit-timber-hammercloud.cern.ch/kibana/app/kibana](https://monit-timber-hammercloud.cern.ch/kibana/app/kibana)
   * HammerCloudAgile: [https://twiki.cern.ch/twiki/bin/viewauth/IT/HammerCloudAgile#Release%20a%20new%20version%20of%20the%20pac](https://twiki.cern.ch/twiki/bin/viewauth/IT/HammerCloudAgile#Release%20a%20new%20version%20of%20the%20pac) to section relevant for building RPM of hammercloud-atlas software
   * HammerCloudTutorialATLASsiteAdmins: [https://twiki.cern.ch/twiki/bin/view/IT/HammerCloudTutorialATLASsiteAdmins](https://twiki.cern.ch/twiki/bin/view/IT/HammerCloudTutorialATLASsiteAdmins)
   * Blacklisting overview at site jamboree 03/2018: [https://indico.cern.ch/event/692124/contributions/2899914/attachments/1611506/2559099/BlacklistingOverview_ATLASSiteJamboree20180306_jschovan.pdf](https://indico.cern.ch/event/692124/contributions/2899914/attachments/1611506/2559099/BlacklistingOverview_ATLASSiteJamboree20180306_jschovan.pdf)
   * Koji client configuration for RPM building: [https://cern.service-now.com/service-portal?id=kb_article&n=KB0005632](https://cern.service-now.com/service-portal?id=kb_article&n=KB0005632)
   * ATLAS HammerCloud code: [https://gitlab.cern.ch/hammercloud/hammercloud-atlas](https://gitlab.cern.ch/hammercloud/hammercloud-atlas)
   * Templates and additional files for ATLAS tests: [https://gitlab.cern.ch/hammercloud/hammercloud-atlas-inputfiles](https://gitlab.cern.ch/hammercloud/hammercloud-atlas-inputfiles)
   * Gitbook docs source code: [https://gitlab.cern.ch/hammercloud/hammercloud-docs](https://gitlab.cern.ch/hammercloud/hammercloud-docs)
   * Code related to nightlies: [https://gitlab.cern.ch/hammercloud/hammercloud-atlas-software](https://gitlab.cern.ch/hammercloud/hammercloud-atlas-software)
   * Legacy HammerCloud operations docs: [https://twiki.cern.ch/twiki/bin/viewauth/IT/HammerCloudATLASOperations](https://twiki.cern.ch/twiki/bin/viewauth/IT/HammerCloudATLASOperations)


