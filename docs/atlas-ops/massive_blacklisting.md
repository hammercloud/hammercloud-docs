Massive blacklisting mitigation
===


# Disable hammercloud probe in CRIC controller
* On Monday 6th Feb 2017 the PanDA controller was decommissioned, and succeeded by AGIS controller: [elog:60136](https://atlas-logbook.cern.ch/elog/ATLAS+Computer+Operations+Logbook/60136).
* In week 45 of 2020 AGIS controller was succeeded by CRIC controller. Remaining mentions of AGIS to replaced.
* In the event of massive blacklisting that should not be happening (e.g. mis-configured DBrelease on cvmfs causes failure of all PFT jobs hence massive blacklisting of all production resources despite other jobs that do not use DBrelease are running happily), ATLAS HC Ops team needs to disable hammercloud probe in CRIC.
    * ATLAS HC Ops team can be reached through e-mail to <atlas-adc-hammercloud-support@cern.ch>.
* How to disable hammercloud probe in CRIC:
    * Visit [https://atlas-cric.cern.ch/core/probe/edit/hammercloud/](https://atlas-cric.cern.ch/core/probe/edit/hammercloud/)
    * Set ``State`` to ``DISABLED``.
    * Click on button ``Check input data``, you'll get to the new page with validation form (header: ``Please confirm the changes to be submitted for object hammercloud``).
     * On the validation form page, review intended changes and confirm with button ``Save & continue``.
        * If you did not change status of the ``hammercloud`` probe, the save button will not be available. 
     * Do not forget to re-enable the ``hammercloud`` probe in CRIC once the cause for massive blacklisting is fixed! Communicate this with ATLAS CRC shifter.
      * **N.B.:** if you do not have probe editing **privileges** in CRIC and think you should possess them, request the privileges: [https://atlas-cric.cern.ch/core/usergrouprequest/create/](https://atlas-cric.cern.ch/core/usergrouprequest/create/) and ask the CRIC team to be added to the ``ATLAS_BLACKLISTING`` user group. You can sign in with CERN SSO or a grid proxy. In the latter case make sure it is associated to you CERN account [here](https://resources.web.cern.ch/resources/Manage/Accounts/MapCertificate.aspx).
     * If a bunch of queues was blacklisted but should not have been, and the ``hammercloud`` probe is disabled in CRIC, the blacklisted queues have to be reviewed and un-blacklisted.




# Kill the test that is causing massive blacklisting
* The test that is failing blacklisting jobs massively will be taken into account for blacklisting, unless its state is set to ``error``. The following script will set the state to desired value. 
* The ATLAS HC Ops  team can revert blacklisting by running a script on any of the ATLAS HC nodes: 

```
### e.g. from [root@hammercloud-ai-11 ~]# 

# TESTID=20132648
# /data/hc/apps/atlas/scripts/server/kill-test-for-massive-blacklisting.sh ${TESTID}  2>&1 | tee log.kill_test.`date +%F.%H%M%S`.log

```




# Revert blacklisting
* If a bunch of queues was blacklisted but should not have been, and the ``hammercloud`` probe is disabled in CRIC, the blacklisted queues have to be reviewed and un-blacklisted.
    * This page contains list of blacklisting/whitelisting events in the past 24 hrs. You can filter there for pattern of job failures, to distinguish between queues that were not blacklisted on accident, but rightfully, and queues that were victim to unfortunate massive blacklisting: [http://hammercloud.cern.ch/hc/app/atlas/robot/blacklisting/](http://hammercloud.cern.ch/hc/app/atlas/robot/blacklisting/)
     * CRIC provides documentation what commands to run to change status of PanDA queues: https://atlas-cric-dev.cern.ch/api/core/

* The ATLAS HC Ops  team can revert blacklisting by running a script on any of the ATLAS HC nodes: 

```
### e.g. from [root@hammercloud-ai-11 ~]# 

# STARTDATE=2017-10-03+07:20
# ENDDATE=2017-10-03+08:00
# /data/hc/apps/atlas/scripts/server/revert-blacklisting.sh ${STARTDATE} ${ENDDATE}  2>&1 | tee log.revert_blacklisting.`date +%F.%H%M%S`.log

```


