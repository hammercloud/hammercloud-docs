#Access
HammerCloud Admin interface is behind CERN SSO. 
Roles in HammerCloud are managed with membership in e-groups. In order to be fully able to edit and submit tests, one should be member of e-groups ``basic``, ``self-approver``,  and ``advanced``, e.g. 

   * [hammercloud-sso-ccc-basic](https://e-groups.cern.ch/e-groups/Egroup.do?tab=3&egroupName=hammercloud-sso-ccc-basic)   
   * [hammercloud-sso-ccc-advanced](https://e-groups.cern.ch/e-groups/Egroup.do?tab=3&egroupName=hammercloud-sso-ccc-advanced) 
   * [hammercloud-sso-ccc-self-approver](https://e-groups.cern.ch/e-groups/Egroup.do?tab=3&egroupName=hammercloud-sso-ccc-self-approver) 

# Getting started 
HammerCloud page for the CCC (cloud computing commissioning) application is [http://hammercloud.cern.ch/hc/app/ccc/](http://hammercloud.cern.ch/hc/app/ccc/). 

Login to HammerCloud administration interface (the "Administration" link in the top right corner of the page) is at [https://hammercloud.cern.ch/hc/admin/login/?next=/hc/admin/ccc/](https://hammercloud.cern.ch/hc/admin/login/?next=/hc/admin/ccc/) .

Use your primary CERN account to login with CERN SSO. It should get you to [https://hammercloud.cern.ch/hc/admin/ccc/](https://hammercloud.cern.ch/hc/admin/ccc/) .

## HC Sites
HammerCloud ``Site`` object is the name of the "endpoint" to which we submit condor jobs. E. g.: ``condorce02.cern.ch:9691``, or ``bigbird99.cern.ch:3018``. 

One can introduce a new HC Site by cloning or adding new Site objects in the Site admin: [https://hammercloud.cern.ch/hc/admin/ccc/site/](https://hammercloud.cern.ch/hc/admin/ccc/site/) . 

## HC Templates
HammerCloud ``Template`` object describes what the test will be doing. 

One can introduce a new HC Template by cloning or adding new Template objects in the Template admin: [https://hammercloud.cern.ch/hc/admin/ccc/template/](https://hammercloud.cern.ch/hc/admin/ccc/template/) . 

Templates consist from two main parts: Template object description in admin, and a set of template files (e.g. ``*.tpl`` or ``*.jdl``), and user files e.g. with condor executable script (e.g. ``*.sh``), tarball with input config files, etc. 

Template files and user files are recorded in GitLab repository [https://gitlab.cern.ch/hammercloud/hammercloud-ccc-inputfiles](https://gitlab.cern.ch/hammercloud/hammercloud-ccc-inputfiles) . 
They are deployed with ``hammercloud-ccc-inputfiles`` RPM from the [ai7-testing](http://linuxsoft.cern.ch/internal/repos/ai7-testing/x86_64/os/Packages/) repository.

On the submit node (e.g. ``hammercloud-ai-93``) they are stored under ``/data/hc/apps/ccc/inputfiles/templates/``, i.e. the path from the GitLab repo pre-fixed with ``/data/hc/``.
 
You may need to edit the ``*.jdl`` or ``*.sh`` files, mainly to make sure the submit file contains all the correct HTCondor parameters that it should, and that the executable script sets the environment correctly, and that it executes what you desire. 

### Editing a HC Template
a list of hints/options: 

for the template in Admin:

* stress template type
* description: any human-readable string
* lifetime: relevant only to "functional" template type, means "I want the test to run N days". ATLAS HC uses lifetime=1day, CMS HC uses lifetime=2 days
* period: when 1 job per "HC Site" finishes, the next one to this "HC Site" is submitted after waiting "period" seconds.
* Active: important for functional templates, not important for stress templates, I set Active=True
* is golden: not relevant for HC CCC. used in HC ATLAS and HC CMS to distinguish very important functional tests (e.g. those used for blacklisting, or site reliability) from other tests
* obsolete
* type: plas for flags, not important here

* jobtemplate: this is where the JDL comes.
in my templates 24/25 I changed it to "atlas_Reco_tf_pile_21.0.23_external_cloud_1.jdl" or "atlas_Reco_tf_pile_21.0.23_external_cloud_2.jdl"
and by "changed to" I mean that I created a new Jobtemplate object(s) with the patch of "atlas/atlas_Reco_tf_pile_21.0.23_external_cloud_1.jdl"

* usercode: this is where the scripts shipped with the job/executable script for condor comes.
in my templates 24/25 I changed it to "atlas/atlas_Reco_tf_pile_21.0.23_external_cloud_1.sh" or "atlas/atlas_Reco_tf_pile_21.0.23_external_cloud_2.sh"
... new Usercode object with path "atlas/atlas_Reco_tf_pile_21.0.23_external_cloud_1.sh"

and placed the files on hammercloud-ai-9[2-4]:
/data/hc/apps/ccc/inputfiles/templates/atlas/atlas_Reco_tf_pile_21.0.23_external_cloud_1.sh
/data/hc/apps/ccc/inputfiles/templates/atlas/atlas_Reco_tf_pile_21.0.23_external_cloud_1.jdl
/data/hc/apps/ccc/inputfiles/templates/atlas/atlas_Reco_tf_pile_21.0.23_external_cloud_2.sh
/data/hc/apps/ccc/inputfiles/templates/atlas/atlas_Reco_tf_pile_21.0.23_external_cloud_2.jdl

* Optionfile: keep null, used for CMS HC
* Metricperm: keep HTCondor
* Input type: keep HTCondor-CE
* Testoption: keep htc
* Gangabin: keep none
* extraargs: this is how we can "override" some fields in the jdl or sh, so that we don't have to create a new set of jdls and shs everytime.
==> as you can see I did not use this option for cloning the templates, will have a look how to template the templates, so that we can change the 2 lines in jdl via extraargs next time.


* host: this is the "primary submit node" where the test lifetime management runs.
for HC CCC wwe have 3 nodes, I picked hammercloud-ai-92, because it is a "single node celery cluster". also, David's template 13's tests run there.

* template sites: this is the "HC site", a class that has condor ce hostname and port defined. Ben asked for condorce02.

* template DSpatterns: inputs for HC tests. relevant for HC ATLAS and HC CMS.
for CCC we fetch data from inside the job, so inputs management is part of "condor executable".





## HC Tests
HammerCloud ``Test`` object describes what the test will be doing, and when, how often. 

One can introduce a new HC Test by cloning or adding new Test objects in the Test admin: [https://hammercloud.cern.ch/hc/admin/ccc/test/](https://hammercloud.cern.ch/hc/admin/ccc/test/) . 

We will play with a clone of a particular template ``FIXME``, and we will try to modify it a bit. 
We will use the ``stress`` test type. In this case we decide when to start and terminate the test, and HammerCloud will not automatically resubmit the test once it is over.


### Editing a HC Test
Template: select the ID 24, and hit "save and continue editing"

the "save and continue editing" will pre-fill many fields of the test.
test class has majority of fields the same as template class.

now e.g. the test admin page is https://hammercloud.cern.ch/hc/admin/ccc/test/1758/change/   
... i.e. test 1758
check
* start/end times
* test host
* test sites: check whether "min queue depth" and "max running jobs" are reasonable for each selected "Site"
max running jobs ... cap on concurrent running jobs
min queue depth ... submit jobs in order to create queue of at least this amount of jobs
having high max running and min queue depth can help to ramp up stress test.

# Submit a new test
You just created a new test. Saving it got you to the test admin page [https://hammercloud.cern.ch/hc/admin/ccc/test/](https://hammercloud.cern.ch/hc/admin/ccc/test/) . 

click checkbox next to your test (mine is 1758)
the test is in status 'draft'.

as long as the test is in 'draft', you can edit it.

and in Actions select the one with "Mark selected tests as approved" and hit "Go"

this will change test status from 'draft' to 'tobescheduled', and HC will schedule it within 4-5 minutes to start running (test management lifecycle) on the primary submit node.

you can see progress on the test page: http://hammercloud.cern.ch/hc/app/ccc/test/1758/


in the Jobs tab you can see condor-ce job IDs and can check yourself 

 the new test is listed on the homepage: http://hammercloud.cern.ch/hc/app/ccc/
 
 
Get a popcorn or coffee & enjoy! 


