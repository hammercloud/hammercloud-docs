# Test monitoring loop

**Monitoring loop:** monitoring loop keeps an eye on submitted/running jobs, and makes sure that desired number of jobs is running at each site (and submits more jobs once the running ones complete). At the end of the test request to cancel non-complete submitted/running jobs is issued. 

## Test monitoring loop in ``celery_crab3api``
* Main script: [apps/cms/python/scripts/runtest_celery_crab3api.py](https://gitlab.cern.ch/hammercloud/hammercloud-cmscrab3/-/blob/cc7/apps/cms/python/scripts/runtest_celery_crab3api.py) 

  * main function: ``RuntestCeleryCrab3api.run()``
* Main wrapper: [/data/hc/scripts/submit/test-run.sh](https://gitlab.cern.ch/hammercloud/hammercloud-core/blob/cc7/hc_core/scripts/submit/test-run.sh#L141)

* Related scripts: 
  * ``from cms.python.scripts.submit.submit_job import JobSubmitter`` [apps/cms/python/scripts/submit/submit_job.py](https://gitlab.cern.ch/hammercloud/hammercloud-cmscrab3/-/blob/cc7/apps/cms/python/scripts/submit/submit_job.py) ... library to submit a job to CRAB3. 
  * ``from cms.python.scripts.submit.tasks import update_job, summarize`` [apps/cms/python/scripts/submit/tasks.py](https://gitlab.cern.ch/hammercloud/hammercloud-cmscrab3/-/blob/cc7/apps/cms/python/scripts/submit/tasks.py) ... celery tasks to 
      * update crab job/subjob status in HC database ==> to e.g. resubmit the CRAB3 task, if all the subjobs are finished, from [apps/cms/python/scripts/submit/update_job_status.py](https://gitlab.cern.ch/hammercloud/hammercloud-cmscrab3/-/blob/cc7/apps/cms/python/scripts/submit/update_job_status.py)
      * summarize the HC test status, for the piechart on the HC test page, and for the numbers of jobs in a test on the main HC page
  * All interaction with CRAB3 is wrapped through [apps/cms/python/scripts/submit/CRABBACKEND.py](https://gitlab.cern.ch/hammercloud/hammercloud-cmscrab3/-/blob/cc7/apps/cms/python/scripts/submit/CRABBACKEND.py)    
