# Test submission

**Test submission:** first bunch of jobs from configuration files are submitted to CRAB3.

## Test submission in ``celery_crab3api``
* Main script: [apps/cms/python/scripts/submittest_simple.py](https://gitlab.cern.ch/hammercloud/hammercloud-cmscrab3/-/blob/cc7/apps/cms/python/scripts/submittest_simple.py)

  * main function: ``SubmittestCeleryCRAB3API.run()``
  * test submission uses celery task ``submit_job`` from [apps/cms/python/scripts/submit/tasks.py](https://gitlab.cern.ch/hammercloud/hammercloud-cmscrab3/-/blob/cc7/apps/cms/python/scripts/submit/tasks.py)
  * All interaction with CRAB3 is wrapped through [apps/cms/python/scripts/submit/CRABBACKEND.py](https://gitlab.cern.ch/hammercloud/hammercloud-cmscrab3/-/blob/cc7/apps/cms/python/scripts/submit/CRABBACKEND.py)    
* Main wrapper: [/data/hc/scripts/submit/test-run.sh](https://gitlab.cern.ch/hammercloud/hammercloud-core/blob/cc7/hc_core/scripts/submit/test-run.sh#L138)

