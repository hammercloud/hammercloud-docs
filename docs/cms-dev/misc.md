# Miscellaneous

## Git branches
We develop in feature branch (name contains your username) and create merge request to ``cc7`` branch. 


## Coding style
We are following [PEP8](https://www.python.org/dev/peps/pep-0008/). In particular:

  * indentation by 4 spaces. no tabs.


## Logging
Use the ``self._logger`` object. e.g. 

```
self._logger.info('this is info message')
self._logger.debug('this is debug message')
self._logger.error('this is error message')
self._logger.critical('this is critical message')

```
Do not use ``print`` statements for logging. 

## Handling exceptions
Use ``try: ... except: ...`` statements. Use the ``traceback`` library. 
Leverage logging to help you trace and later reproduce issues with your code. 

## Locality of code execution
Some parts of the code of test lifetime are executed on the chosen submit node (the one listed on the test page, e.g. ``hammercloud-ai-21``), other parts can be executed on any node of the submit cluster ("distributed parts"). 

The "distributed parts" of the code are executed as [Celery](http://www.celeryproject.org/) tasks. 
A Celery task is a function in python. All tasks are listed in [apps/cms/python/scripts/submit/tasks.py](https://gitlab.cern.ch/hammercloud/hammercloud-cmscrab3/-/blob/cc7/apps/cms/python/scripts/submit/tasks.py). 

Celery tasks are executed asynchronously, and return value is of type ``AsyncResult``, i.e. not the return value you expect. Therefore use Celery tasks only if the nature of the task allows you to not have to wait for the return value/result.

Celery tasks are called with `.delay(...)` method. The input parameter of the task are listed as input parameters of the `.delay()` method. 

## Monitoring of the celery cluster status
 * Celery cluster status monitoring is possible from the Celery Flower monitor, for each node it is available on port ``:5555``, e.g. http://hammercloud-ai-20:5555/dashboard
     * Overview dashboard is in the ``Dashboard`` tab, e.g. http://hammercloud-ai-20:5555/dashboard 
     * Throughput plots are in the ``Monitor`` tab, e.g. http://hammercloud-ai-20:5555/monitor
     * Details about the celery tasks are in the ``Tasks`` tab, e.g. http://hammercloud-ai-20:5555/tasks
* Assignment to a redis node is done in Foreman, a node configuration. This can be done by e.g. the IT-CM-IS engineering rota members, please contact us via a SNOW ticket, the ``WLCG HammerCloud`` FE: https://cern.service-now.com/service-portal/?id=functional_element&name=WLCG-HammerCloud
* If a huge backlog evolves in the celery queue, in the ``Monitor`` the plot Queued tasks will show that, and in the main celery log file, ``/var/log/celery_hcuser/daemon.worker.celery.CMSSW_9_2_6.daemon.worker.celery.CMSSW_9_2_6.log`` one can see updates of jobs which belong to already expired HC tests, please ask the engineering rota for a celery queue purge: [https://twiki.cern.ch/twiki/bin/view/IT/HammerCloudRota#Force_purge_of_a_celery_queue_ba](https://twiki.cern.ch/twiki/bin/view/IT/HammerCloudRota#Force_purge_of_a_celery_queue_ba)

