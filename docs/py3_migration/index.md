# Python 3.8 migration

## Python3 node
* node: hammercloud-ai-10
* python3.8
* Django 4

## Python3 code
* branch: ``cc7_py3`` in hammercloud-atlas and hammercloud-core repos

## Python3 migration steps
* try to make the ``submit-main.sh atlas`` work. 
* try to make the AGIS/CRIC collector work (``update_from_agis4.py``)
* try to make the ``test-run.sh`` work to be able to run ``run-test-$TESTID,sh``.
* try to make the blacklisting scripts work.


## Commissioning ``submit-main.sh atlas``
Steps:

```
# ssh $USER@aivoadm.cern.ch
# ssh $USER@hammercloud-ai-10
# sudo su
# su - hcuser

# /data/hc/scripts/submit/submit-main.sh atlas
Setting up HammerCloud core environment...
 HCDIR=/data/hc
 APP=atlas
 HCAPP=/data/hc/apps/atlas
 SL_RELEASE=
 PATH=/usr/sue/bin:/usr/share/Modules/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/puppetlabs/bin
 LD_LIBRARY_PATH=
 * Running on Python 3.8.12
 * Running on GCC 8.5.0 
 PYTHONPATH=/data/hc/python:/data/hc/apps:/data/hc/web/src:
 DJANGO_SETTINGS_MODULE=hc.settings
Setting up HammerCloud ATLAS environment...
 HC_MODE=default
 X509_USER_PROXY=/data/hc/apps/atlas/config/x509rp
Sourcing PanDA tools from local install... 
Sourcing locally installed DQ2 client...
Launching the register_host action...
INFO 2022-01-21 00:49:07,642 register_host 182590 140696674337024 Inserted new load for hammercloud-ai-10.cern.ch: 0.008327 (new value: 0.040000)
Launching the create_at_job action...
[INFO][atlas][create_at_job] No tests found on state: tobescheduled
Traceback (most recent call last):
  File "/data/hc/python/scripts/dispatcher.py", line 78, in <module>
    sys.exit(run(dic))
  File "/data/hc/python/scripts/dispatcher.py", line 64, in run
    getattr(ca, dic['-f'])(app, dic)
  File "/data/hc/python/scripts/actions.py", line 97, in create_at_job
    caj.run(app, dic)
  File "/data/hc/python/scripts/submit/create_at_job.py", line 318, in run
    tests = test.objects.filter(state='running').filter(host__name=hostname)
  File "/usr/local/lib64/python3.8/site-packages/django/db/models/query.py", line 974, in filter
    return self._filter_or_exclude(False, args, kwargs)
  File "/usr/local/lib64/python3.8/site-packages/django/db/models/query.py", line 992, in _filter_or_exclude
    clone._filter_or_exclude_inplace(negate, args, kwargs)
  File "/usr/local/lib64/python3.8/site-packages/django/db/models/query.py", line 999, in _filter_or_exclude_inplace
    self._query.add_q(Q(*args, **kwargs))
  File "/usr/local/lib64/python3.8/site-packages/django/db/models/sql/query.py", line 1375, in add_q
    clause, _ = self._add_q(q_object, self.used_aliases)
  File "/usr/local/lib64/python3.8/site-packages/django/db/models/sql/query.py", line 1396, in _add_q
    child_clause, needed_inner = self.build_filter(
  File "/usr/local/lib64/python3.8/site-packages/django/db/models/sql/query.py", line 1271, in build_filter
    lookups, parts, reffed_expression = self.solve_lookup_type(arg)
  File "/usr/local/lib64/python3.8/site-packages/django/db/models/sql/query.py", line 1099, in solve_lookup_type
    _, field, _, lookup_parts = self.names_to_path(lookup_splitted, self.get_meta())
  File "/usr/local/lib64/python3.8/site-packages/django/db/models/sql/query.py", line 1522, in names_to_path
    raise FieldError("Cannot resolve keyword '%s' into field. "
django.core.exceptions.FieldError: Cannot resolve keyword 'host' into field. Choices are: atjobid, cloned, description, endtime, extraargs, id, is_golden, mtime, output_dataset, pause, period, processing_meta, starttime, state, version

```
... this error means that our historical foreign keys handling may not be working. 

Check ``hc_web/web/src/hc/core/base/models/keys/fk/generator.py`` and ``hc_web/web/src/hc/core/base/models/keys/m2m/generator.py`` to see if anything can be done. 

Check how Django 4 handles foreign keys ==> will we need to do any changes to the DB schema?


)

)